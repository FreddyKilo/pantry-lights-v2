# Pantry Lights

Pantry Lights is a micropython application that runs on a WEMOS D1 mini (ESP8266) IoT device.
It dynamically controls an array of LED pantry lights when sunset or sunrise occurs, and when
motion has been detected. All switching and dimming is controlled by pulse width modulation
with a night-light mode that is active after sunset. Sunrise and sunset times are specific to
the hardware's geolocation.
 

## Tools and resources
* Helpful development tools
    * PyCharm IDE - www.jetbrains.com/pycharm/download/
    * PyCharm micropython plugin
    * webrepl - https://github.com/micropython/webrepl
    * Windows CH340 USB driver - https://sparks.gogo.co.nz/ch340.html
    * GitKraken - https://www.gitkraken.com/git-client
* Helpful resources
    * MicroPython documentation - https://docs.micropython.org/en/latest/
    * References and tutorials for the ESP8266 - https://docs.micropython.org/en/latest/esp8266/quickref.html
    * WEMOS D1 mini documentation - https://docs.wemos.cc/en/latest/d1/d1_mini.html
    
## Prerequisites
* Get a WEMOS from your favorite online mega store - https://www.amazon.com/s?k=wemos+d1+mini&ref=nb_sb_noss
* Download the MicroPython firmware - http://micropython.org/download/esp8266/
* If using Windows, or older versions of macOS, install the CH340 USB driver. This will allow you to upload firmware and files to the device as well as capture output logs and use the python REPL
* Install `esptool.py` to flash firmware - run `pip install esptool` from a command line
* Install `ampy`

## Notes
* `esptool.py` is used to write or erase the MicroPython image to the chip. For more info run `esptool.py -h`
* `ampy` is used to manage the file system on the chip. For more info run `ampy --help`

## Firmware installation
1. Open PyCharm and create a new project
1. Install the micropython plugin
1. Using a micro-USB data cable—this *has* to be a confirmed data cable to be able to communicate with the device—plug the WEMOS into your computer
1. In Pycharm > Settings > Languages & Frameworks > MicroPython, select your project and enable "Auto-detect device path". This will display the port name needed to use with esptool.py to flash the Wemos
1. Flash the MicroPython firmware onto your device
     1. run `esptool.py --port PORT_NAME erase_flash`
     1. run `esptool.py --port PORT_NAME --baud 1000000 write_flash --flash_size=4MB -fm dio 0 FIRMWARE.bin`