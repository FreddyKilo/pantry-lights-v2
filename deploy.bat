@echo off

set port=COM3

echo Deploying application...
echo Flashing boot.py, main.py...
ampy -p %port% put src\boot.py
ampy -p %port% put src\main.py

echo Removing \app...
ampy -p %port% rmdir app

echo Flashing \app...
ampy -p %port% put src\app

echo Files on the device:
ampy -p %port% ls
ampy -p %port% ls app
echo;

echo Don't forget to push the reset button to start application!
echo;
