import json
import urequests as requests

DATETIME_URL = "http://worldtimeapi.org/api/ip"
SUNRISE_SUNSET_URL = "https://api.sunrise-sunset.org/json?lat={lat}&lng={lng}&formatted=0"


def get_datetime():
    print("\ngetting datetime...")

    resp = requests.get(DATETIME_URL)
    body = json.loads(resp.text)
    print(body)

    return __map_datetime(body)


def get_sunrise_sunset(latitude, longitude, utc_offset, dst_offset):
    print("\ngetting sunrise/sunset...")

    resp = requests.get(SUNRISE_SUNSET_URL.format(lat=latitude, lng=longitude))
    body = json.loads(resp.text)
    print(body)

    return __map_sunrise_sunset(body, utc_offset, dst_offset)


def __map_datetime(response):
    time = response['datetime'].split('T')[1].split('.')[0]

    return {
        'hour': int(time.split(':')[0]),
        'minute': int(time.split(':')[1]),
        'utc_offset': int(response['utc_offset'].split(':')[0]),
        'dst_offset': response['dst_offset'],
        'day_of_year': response['day_of_year']
    }


def __map_sunrise_sunset(response, utc_offset, dst_offset):
    sunrise = response['results']['sunrise'].split('T')[1].split('+')[0]
    sunset = response['results']['sunset'].split('T')[1].split('+')[0]

    return {
        'sunrise': {
            'hour': (int(sunrise.split(':')[0]) + 24 + utc_offset + dst_offset) % 24,
            'minute': int(sunrise.split(':')[1])
        },
        'sunset': {
            'hour': (int(sunset.split(':')[0]) + 24 + utc_offset + dst_offset) % 24,
            'minute': int(sunset.split(':')[1])
        }
    }
