from machine import Pin, PWM
from utime import sleep_ms, sleep_us

from app.pin_mapper import Wemos


class Controller:
    SUNSET_DIMMER_VALUE = 64

    def __init__(self):
        self.lights_pin = PWM(Pin(Wemos.D1, Pin.OUT), freq=1000)
        self.interrupted_by = []
        self.sunset_status = False

    def lights_on(self, value=1023):
        self.__fade_in(self.lights_pin, value)

    def lights_off(self, value=0):
        if self.sunset_status:
            value = self.SUNSET_DIMMER_VALUE
        self.__fade_out(self.lights_pin, value)

    '''
    Set the condition of an input or output pin state that should interrupt the fade out
    '''
    def set_interrupted_by(self, pin, value):
        self.interrupted_by.append({
            'pin': pin,
            'value': value
        })

    def set_sunset_status(self, sunset_status):
        self.sunset_status = sunset_status

    def __fade_in(self, pwm_pin, high_value=1023):
        for i in range(pwm_pin.duty(), high_value):
            pwm_pin.duty(i)
            sleep_us(100)

    def __fade_out(self, pwm_pin, low_value=0):
        high = pwm_pin.duty()
        for i in range(high - low_value):
            pwm_pin.duty(high - i - 1)
            sleep_ms(2)
            if self.__is_interrupted():
                return

    def __is_interrupted(self):
        for interrupter in self.interrupted_by:
            if interrupter['pin'].value() == interrupter['value']:
                return True

        return False


class Const:
    ONE_MINUTE = 60
    MINUTES_PER_DAY = 1440
    SUNSET_MONITOR_SLEEP_MINUTE_INTERVAL = 120
