def loop(func):
    def looper(*args):
        while True:
            await func(*args)

    return looper
