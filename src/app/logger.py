import urequests as requests

HOST = "https://dweet.io"
INFO_PATH = "/dweet/for/plv2-1-info"
ERROR_PATH = "/dweet/for/plv2-1-error"


def info(json_body):
    requests.post(HOST + INFO_PATH, json=json_body)


def error(json_body):
    requests.post(HOST + ERROR_PATH, json=json_body)
