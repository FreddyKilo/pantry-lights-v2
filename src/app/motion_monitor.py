import uasyncio

from machine import Pin
from app.decorators import *


class MotionMonitor:
    def __init__(self, controller, pinout):
        self.__controller = controller
        self.ir_pin = Pin(pinout, Pin.IN)
        controller.set_interrupted_by(self.ir_pin, 1)

    @loop
    async def run(self):
        if self.is_motion_detected():
            self.__controller.lights_on()
        else:
            self.__controller.lights_off()
        await uasyncio.sleep_ms(30)

    def is_motion_detected(self):
        return self.ir_pin.value()
