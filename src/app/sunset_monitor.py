import uasyncio
from app import logger

from app.decorators import *
from app.controller import Const
from app.clients import *


class SunsetMonitor:
    LATITUDE = '33.3354'
    LONGITUDE = '-111.8153'
    SUNRISE = 'sunrise'
    SUNSET = 'sunset'

    def __init__(self, controller):
        self.__controller = controller
        self.__datetime = {}
        self.__day_of_year = 0
        self.__sunrise_sunset_data = {}
        self.sun_has_set = False

    def time_until_sunrise(self):
        sunrise_time = self.get_sunrise_time()
        time = (sunrise_time + Const.MINUTES_PER_DAY - self.get_current_time()) % Const.MINUTES_PER_DAY
        print("time until sunrise: " + str(time))

        return time

    def time_until_sunset(self):
        sunset_minute = self.get_sunset_time()
        time = sunset_minute - self.get_current_time()
        print("time until sunset: " + str(time))

        return time

    def get_current_time(self):
        hour = self.__datetime['hour']
        minute = self.__datetime['minute']

        return self.to_minutes(hour, minute)

    def get_sunrise_time(self):
        hour = self.__sunrise_sunset_data['sunrise']['hour']
        minute = self.__sunrise_sunset_data['sunrise']['minute']

        return self.to_minutes(hour, minute)

    def get_sunset_time(self):
        hour = self.__sunrise_sunset_data['sunset']['hour']
        minute = self.__sunrise_sunset_data['sunset']['minute']

        return self.to_minutes(hour, minute)

    def to_minutes(self, hour, minute):
        return int(hour) * 60 + int(minute)

    def is_sunset(self):
        try:
            if self.__datetime['day_of_year'] != self.__day_of_year:

                self.__sunrise_sunset_data = get_sunrise_sunset(self.LATITUDE, self.LONGITUDE, self.__datetime['utc_offset'], self.__datetime['dst_offset'])
                self.__day_of_year = self.__datetime['day_of_year']

            sunrise = self.get_sunrise_time()
            current_time = self.get_current_time()
            sunset = self.get_sunset_time()
            self.sun_has_set = not sunrise <= current_time < sunset

            logger.info({
                'sunrise': str(sunrise),
                'currentTime': str(current_time),
                'sunset': str(sunset),
                'isSunset': str(self.sun_has_set)
            })

            return self.sun_has_set

        except Exception as e:
            exception = repr(e)
            print("[ERROR] is_sunset: " + exception)
            logger.error({'message': "error getting sunset data in is_sunset", 'exception': exception})
            raise e

    @loop
    async def run(self):
        sleep_time = Const.SUNSET_MONITOR_SLEEP_MINUTE_INTERVAL

        try:
            self.__datetime = get_datetime()

            if self.is_sunset():
                self.__controller.set_sunset_status(True)
                self.__controller.lights_on(self.__controller.SUNSET_DIMMER_VALUE)

                time_to_sunrise = self.time_until_sunrise()
                if time_to_sunrise < Const.SUNSET_MONITOR_SLEEP_MINUTE_INTERVAL:
                    sleep_time = time_to_sunrise
            else:
                self.__controller.set_sunset_status(False)
                self.__controller.lights_off()

                time_to_sunset = self.time_until_sunset()
                if time_to_sunset < Const.SUNSET_MONITOR_SLEEP_MINUTE_INTERVAL:
                    sleep_time = time_to_sunset

        except Exception as e:
            exception = repr(e)
            print("[ERROR] SunsetMonitor.run: " + exception)
            logger.error({'message': "error in SunsetMonitor.run, trying again in 10 minutes", 'exception': exception})
            sleep_time = 10

        if sleep_time < 1:
            sleep_time = 1

        print("sleep time: " + str(sleep_time))
        await uasyncio.sleep(Const.ONE_MINUTE * sleep_time)
