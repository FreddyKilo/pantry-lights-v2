import json
import network
import utime
from machine import Pin, Signal


def boot():
    wlan = network.WLAN(network.STA_IF)
    if connect_to_wlan(wlan):
        indicate_connect_success(wlan)
        return

    indicate_connect_failure(wlan)


def connect_to_wlan(wlan):
    wlan.active(True)

    try:
        if not wlan.isconnected():
            print("attempting to connect to router...")
            wifi = get_wifi_credentials()
            wlan.connect(wifi["ssid"], wifi["password"])

        # Give a max of 10 seconds to connect
        for i in range(10):
            utime.sleep(1)
            if wlan.isconnected():
                break
    except:
        return False

    return wlan.isconnected()


def get_wifi_credentials():
    file = open("wifi-credentials.json")
    data = json.loads(file.read())
    file.close()
    return data["wifi"]


def indicate_connect_success(wlan):
    print("status: ", wlan.status())
    print("network config: ", wlan.ifconfig())
    blink(3, 500)


def indicate_connect_failure(wlan):
    print("could not connect to network")
    print("status: ", wlan.status())
    blink(200, 200)


def blink(count, blink_interval_ms):
    pin = Pin(2, Pin.OUT)
    led = Signal(pin, invert=True)
    switch = True
    while count > 0:
        if switch:
            led.on()
        else:
            led.off()
            count -= 1
        utime.sleep_ms(blink_interval_ms)
        switch = not switch


if __name__ == '__main__':
    boot()
