import uasyncio

from app.pin_mapper import Wemos
from app.controller import Controller
from app.sunset_monitor import SunsetMonitor
from app.motion_monitor import MotionMonitor


def main():
    controller = Controller()
    sunset_monitor = SunsetMonitor(controller)
    motion_monitor = MotionMonitor(controller, Wemos.D2)

    event_loop = uasyncio.get_event_loop()
    event_loop.create_task(sunset_monitor.run())
    event_loop.create_task(motion_monitor.run())
    event_loop.run_forever()


if __name__ == '__main__':
    main()
