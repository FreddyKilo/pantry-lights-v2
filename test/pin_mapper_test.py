import unittest

from src.app.pin_mapper import Wemos


class PinMapperTest(unittest.TestCase):

    def test_pinouts(self):
        self.assertEqual(16, Wemos.D0)
        self.assertEqual(2, Wemos.ESP_LED)


if __name__ == '__main__':
    unittest.main()
